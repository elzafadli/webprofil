<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0*Bg&F)Y*W0#y`M66.T*h2Cm6NX<;87kQFE_CG?cvft3hA7y{Hz_R|kW< ]n@yo=');
define('SECURE_AUTH_KEY',  '+$?{W/;=K*fNJ<D$(`<>e?d=&Q$6BDKD*HaoB}X|!j*%_{&#2~7msD>=YtdVN?#p');
define('LOGGED_IN_KEY',    'o[WR&V#A$&6c4of9zHfrh/p+KStl!7Ib$}0 4M[WKm3l(g;<t)QNXaFbtB%.!P}r');
define('NONCE_KEY',        '|c^O[kz=<Yms52P+Rhf~Qg-+JWNC]A/~*C/]8(!_[L7|9Mc{w/mSCMKv|q%2=WgB');
define('AUTH_SALT',        '2_K@xTg2?,g:H})^)+G*Y!^e-9JbFj-{RT2UE@[MNsxdL^,8[<PK]Kj`A7QQh[H%');
define('SECURE_AUTH_SALT', '<Lst|jm$in7hU,plE|@/49bwEEl$q;-9_ng_/_zba#`qPL^f]y>s+eb(9=M]k7%`');
define('LOGGED_IN_SALT',   '-0h,9)52V&9D05|$~+NOk9K^wo=<I!~bn%us_vS6V+1Oq%#9_Sl@m?G&R>^) FG.');
define('NONCE_SALT',       '2 W*CvbT?OV}F@tLY47kof}(SsmQxzob|n{nORo;GyBMB(Ys|-W?29=]Vq`V>J0X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
