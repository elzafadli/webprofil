<?php get_header(); ?>
    
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>

  <?php endwhile; else: ?>
    <p><?php _e('Sorry, this page does not exist.'); ?></p>
  <?php endif; ?>

<?php include_once( dirname(__FILE__) . '/addon/slider.php'); ?>

<?php get_footer(); ?>
