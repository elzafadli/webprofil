<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<div class="row">
				<div class="col-sm-8" style="background-color: lavender;">
				<h1><?php the_title(); ?></h1>

    <p><em><?php the_time('l, F jS, Y'); ?></em></p>
					<hr />
					<div style="text-align: justify;">
			  	<?php the_content(); ?>
					</div>
			</div>

				<div class="col-sm-4" style="background-color:#bdc3c7;border-radius:5px;padding:15px">
					<div >
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>

	<?php endwhile; else: ?>
		<p><?php _e('Sorry, this page does not exist.'); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
