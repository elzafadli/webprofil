<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>


<form role="search" method="get" id="searchform"
    class="form-group" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div>
        <label for="exampleInputEmail1">Search :</label>
        <input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" />
				<br>
        <input type="submit" id="searchsubmit" class="btn btn-primary"
            value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
    </div>
</form>

<h3>Recent Posts :</h3>
  <ul class="list-group">
<?php
    $recent_posts = wp_get_recent_posts();
    foreach( $recent_posts as $recent ) {
        printf( '<li class="list-group-item"><a href="%1$s">%2$s</a></li>',
            esc_url( get_permalink( $recent['ID'] ) ),
            apply_filters( 'the_title', $recent['post_title'], $recent['ID'] )
        );
    }
?>
</ul>
<hr>
<?php
$categories = get_categories( array(
    'orderby' => 'name',
    'order'   => 'ASC'
) );
?>
<h3>Kategori :</h3>
  <ul class="list-group">
<?php
foreach( $categories as $category ) {
    $category_link = sprintf(
        '<li class="list-group-item"><a href="%1$s" alt="%2$s">%3$s</a></li>',
        esc_url( get_category_link( $category->term_id ) ),
        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
        esc_html( $category->name )
    );

    echo '<p>' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . '</p> ';
}

?>
