&nbsp;
<h1><div><center><strong>Ruang Lingkup</strong></center></div></h1>
&nbsp;

&nbsp;
<div class="section-2" style="background-color: #3498db; position: relative;">
<div class="container">
<div class="row" style="margin: 50px 0; color: white;">
<div class="col-sm-4" style="border-left: 1px solid #bdc3c7; border-right: 1px solid #bdc3c7; padding: 15px 20px; border-radius: 10px;"><center><i class="fa fa-gears fa-5x" onClick="document.getElementById('solusi').scrollIntoView()" aria-hidden="true"></i></center>
<center><h4 style="color: #333;" onClick="document.getElementById('solusi').scrollIntoView()">Solusi TI</h4>
<p style="text-align: justify;">Solusi bisnis anda dalam pengembangan TI, baik itu dalam hal pengembangan perangkat lunak (software) maupun pengembangan perangkat keras (hardware).</p>

</center></div>
<div class="col-sm-4" style="border-left: 1px solid #bdc3c7; border-right: 1px solid #bdc3c7; padding: 15px 20px; border-radius: 10px;"><center><i class="fa fa-users fa-5x"onClick="document.getElementById('solusi').scrollIntoView()" aria-hidden="true"></i></center>
<center><h4 style="color: #333;" onClick="document.getElementById('solusi').scrollIntoView()">Konsultan TI</h4>
</center>
<p style="text-align: justify;">Konsultasikan bisnis anda dalam pengembangan TI, baik itu dalam hal pengembangan perangkat lunak (software) maupun pengembangan perangkat keras (hardware).</p>

</div>
<div class="col-sm-4" style="border-left: 1px solid #bdc3c7; border-right: 1px solid #bdc3c7; padding: 15px 20px; border-radius: 10px;"><center><i class="fa fa-graduation-cap fa-5x"onClick="document.getElementById('latihan').scrollIntoView()" aria-hidden="true"></i></center>
<center><h4 style="color: #333;" onClick="document.getElementById('latihan').scrollIntoView()">Pendidikan &amp; Pelatihan TI</h4>
<p style="text-align: justify;">Kami siap memberikan pelatihan dan pendampingan dalam proses penelitian baik yang dilakukan oleh mahasiswa maupun pihak dari luar / umum.<br></p>

</center></div>
</div>
</div>
</div>
<div class="container" style="margin-top: 60px; margin-bottom: 55px;">
<div class="row">
<div class="col-sm-12" style="background-color: lavender;">
<h1>Ruang Lingkup</h1>

<hr />

<div id="solusi"></div>
<h3>SOLUSI BISNIS &amp; KONSULTAN TI (IT BUSINESS SOLUTION &amp; CONSULTANT)</h3>
Sarana Utama Solusindo siap menjadi partner anda dalam pengembangan TI baik itu dalam hal pengembangan perangkat lunak (<em>software</em>) maupun pengembangan perangkat keras (<em>hardware</em>). Beberapas fokus dari CV. Sarana Utama Solusindo adalah:
<ol>
 	<li>Pengembangan Sistem Informasi Manajemen (<em>Management Information System</em>)</li>
</ol>
<ul>
 	<li>Sistem Informasi Puskesmas (SIMAS) – <em>on going</em></li>
 	<li>Sistem informasi ini merupakan sistem informasi alur proses pengobatan yang ada di puskesmas.</li>
 	<li>Sistem Informasi Geografis Puskesmas (SIMAS) – <em>on going</em></li>
 	<li>Sistem informasi ini berbasis geografis dengan melakukan pemetaan wilayah menurut alamat dari pasien. Pemetaan dilakukan berdasarkan poin-poin tertentu yang ingin diketahui salah satunya berdasarkan penyakit yang diderita pasien.</li>
 	<li><em>Command Center</em> (Ngalam <em>Command Center</em>) – <em>on going</em></li>
 	<li>Sistem informasi yang berfungsi untuk mengetahui informasi semua kegiatan yang ada di beberapa dinas pemerintahan secara terpusat.</li>
 	<li>Smart City – <em>on going</em></li>
 	<li>Sistem yang digunakan untuk memudahkan kegiatan yang ada di dalam pemerintahan yang berbasis teknologi.</li>
</ul>
<ol start="2">
 	<li>Pengembangan Aplikasi Dekstop &amp; Perangkat Bergerak (Dekstop &amp; <em>Mobile Application</em>)</li>
</ol>
<ul>
 	<li>Aplikasi Antrean Puskesmas – <em>on going</em></li>
 	<li>Pengembangan Perangkat Cerdas (<em>Smart Device</em>)</li>
 	<li>Deteksi Dini Bahaya Banjir dan Banjir Rob (<em>Flood Early Warning System</em>) – <em>completed </em></li>
 	<li>Deteksi Dini Bahaya Tsunami (<em>Tsunami Early Warning System</em>) – <em>completed </em></li>
 	<li>Deteksi Dini Bahaya Tanah Longsor (<em>Landslide Early Warning System</em>) – <em>on going</em></li>
 	<li>Pemantauan Cuaca dan Klimatologi – <em>completed </em></li>
</ul>
<div>

<hr />

<div id="latihan"></div>
<h3>Pelatihan TI (IT <em>Training</em>)</h3>
Sarana Utama Solusindo siap memberikan pelatihan dan pendampingan dalam proses penelitian baik yang dilakukan oleh mahasiswa maupun pihak dari luar / umum. Beberapa pelatihan yang dilakukan oleh CV. Sarana Utama Solusindo adalah:
<ol>
 	<li>Pelatihan penelitian berbasis Kecerdasan Buatan (<em>Artificial Intelligence</em>)</li>
</ol>
<ul>
 	<li><em>Data Mining</em></li>
 	<li><em>Forecasting </em>(Peramalan)</li>
 	<li><em>Decission Support System</em> (Sistem Pendukung Keputusan)</li>
 	<li><em>Expert System </em>(Sistem Pakar)</li>
 	<li><em>Optimization Algorithm</em> (Algoritma Optimasi)</li>
 	<li><em>Computer Vision</em></li>
 	<li>dll</li>
</ul>
<ol start="2">
 	<li>Pelatihan penelitian berbasis Rekayasa Perangkat Lunak (<em>Software Engineering</em>)</li>
</ol>
<ul>
 	<li>Aplikasi Dekstop (<em>Dekstop Application</em>)</li>
 	<li>Aplikasi Perangkat Bergerak (<em>Mobile Application</em>)</li>
 	<li>A0070likasi Berbasis Web (<em>Web Application</em>)</li>
</ul>

<hr />
<div>
  <br
  <div class="w3-content" style="max-width:800px;height:40px;">
    <img class="latihan-1" src="http://localhost/webtest/wp-content/uploads/2017/09/test3.jpg" style="width:100%;height:500px">
    <img class="latihan-1" src="http://localhost/webtest/wp-content/uploads/2017/09/test1.jpg" style="width:100%;height:500px">
    <img class="latihan-1" src="http://localhost/webtest/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-05-at-11.40.39-AM-1.jpeg" style="width:100%;height:500px">
    <img class="latihan-1" src="http://localhost/webtest/wp-content/uploads/2017/09/WhatsApp-Image-2017-09-05-at-11.39.53-AM-1.jpeg" style="width:100%;height:500px">
    <img class="latihan-1" src="http://localhost/webtest/wp-content/uploads/2017/09/167611-1.jpg" style="width:100%;height:500px">
  </div>

  <div class="w3-center">
    <div class="w3-section">
      <button class="w3-button w3-light-grey" onclick="plusDivs(-1)">< Prev</button>
      <button class="w3-button w3-light-grey" onclick="plusDivs(1)">Next ></button>
    </div>
  </div>

  
</div>
</div>
<p class="col-sm-4" style="background-color: lavenderblush;"></p>

</div>
</div>
<!--?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?-->
<!--?php the_content(); ?-->
<!--?php endwhile; else: ?-->

&nbsp;

&nbsp;

&nbsp;
