<?php

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

show_admin_bar(false);



function SearchFilter($query) {
    if ($query->is_search) {
    $query->set('post_type', 'post');
    }
    return $query;
    }

    add_filter('pre_get_posts','SearchFilter');

		/* Register shortcode_atts_gallery filter callback */
		add_filter( 'shortcode_atts_gallery', 'meks_gallery_atts', 10, 3 );

		/* Change attributes of wp gallery to modify image sizes for your needs */
		function meks_gallery_atts( $output, $pairs, $atts ) {

		/* You can use these sizes:
		- thumbnail
		- medium
		- large
		- full
		or, if your theme/plugin generate additional custom sizes you can use them as well
		*/

		$output['size'] = 'medium'; //i.e. This will change all your gallery images to "medium" size

		return $output;

		}


?>

