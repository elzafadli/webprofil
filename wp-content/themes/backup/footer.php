    <div class="footer" style="background-color:rgb(14, 26, 46);padding:10px;0px;color:rgb(204, 204, 204);margin-top:30px;">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h5>Latest News</h5>
              <ul>
              <?php
                $args = array( 'numberposts' => '3' );
	              $recent_posts = wp_get_recent_posts( $args );
              	foreach( $recent_posts as $recent ){
              		echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
              	}
              	wp_reset_query();
              ?>
              </ul>
              <br>
              <br>
              <br>
          </div>
          <div class="col-sm-3">
            <h5>Contact Info</h5>
            <p>cs.sasindo@gmail.com</p>
          </div>
          <div class="col-sm-3">
            <h5>Follow us</h5>
              <i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;
              <i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;
              <i class="fa fa-instagram" aria-hidden="true"></i>&nbsp;
              <i class="fa fa-google" aria-hidden="true"></i>&nbsp;
          </div>
        </div>
      </div>
    </div>

    <div class="footer" style="background-color:rgb(7, 14, 46);color:rgb(204, 204, 204);padding:10px;0px">
      <div class="container">
        <div class="row">
          <div class="col-sm-8">
          </div>
          <div class="col-sm-4">
            <p>-</p>
          </div>
        </div>
      </div>
    </div>

    <?php wp_footer(); ?>

  </body>
</html>
